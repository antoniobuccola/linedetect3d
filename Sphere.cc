#include "Sphere.h"

#include "TMath.h"

ClassImp(Sphere);

Sphere::Sphere()
{
 create_icosahedron();
 make_unique();
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/

Sphere::Sphere(int subdivisions)
{
 create_icosahedron();
 
 for(int k=0; k<subdivisions; k++)
   subdivide();
 
 make_unique();
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/

void Sphere::create_icosahedron()
{
 vertices.clear();
 triangles.clear();

 float tau = 1.61803399; // golden_ratio
 float norm = TMath::Sqrt(1 + tau*tau);
 float v = 1./norm;
 tau /= norm;
 
 vertices.emplace_back(-v, tau, 0); // 
 vertices.emplace_back(v, tau, 0);  // 
 vertices.emplace_back(0, v, -tau); // 
 vertices.emplace_back(0, v, tau);  // 4
 
 vertices.emplace_back(-tau, 0, -v); // 
 vertices.emplace_back(tau, 0, -v);  // 
 vertices.emplace_back(-tau, 0, v);  // 
 vertices.emplace_back(tau, 0, v);   // 8
 
 vertices.emplace_back(0, -v, -tau); // 
 vertices.emplace_back(0, -v, tau);  // 
 vertices.emplace_back(-v, -tau, 0); //
 vertices.emplace_back(v, -tau, 0);  // 12


 triangles.push_back(0);
 triangles.push_back(1);
 triangles.push_back(2); // 1
 triangles.push_back(0);
 triangles.push_back(1);
 triangles.push_back(3); // 2
 triangles.push_back(0);
 triangles.push_back(2);
 triangles.push_back(4); // 3
 triangles.push_back(0);
 triangles.push_back(4);
 triangles.push_back(6); // 4
 triangles.push_back(0);
 triangles.push_back(3);
 triangles.push_back(6); // 5
 triangles.push_back(1);
 triangles.push_back(2);
 triangles.push_back(5); // 6
 triangles.push_back(1);
 triangles.push_back(3);
 triangles.push_back(7); // 7
 triangles.push_back(1);
 triangles.push_back(5);
 triangles.push_back(7); // 8
 triangles.push_back(2);
 triangles.push_back(4);
 triangles.push_back(8); // 9
 triangles.push_back(2);
 triangles.push_back(5);
 triangles.push_back(8); // 10
 triangles.push_back(3);
 triangles.push_back(6);
 triangles.push_back(9); // 1
 triangles.push_back(3);
 triangles.push_back(7);
 triangles.push_back(9); // 12
 triangles.push_back(4);
 triangles.push_back(8);
 triangles.push_back(10); // 13
 triangles.push_back(8);
 triangles.push_back(10);
 triangles.push_back(11); // 14
 triangles.push_back(5);
 triangles.push_back(8);
 triangles.push_back(11); // 15
 triangles.push_back(5);
 triangles.push_back(7);
 triangles.push_back(11); // 16
 triangles.push_back(7);
 triangles.push_back(9);
 triangles.push_back(11); // 17
 triangles.push_back(9);
 triangles.push_back(10);
 triangles.push_back(11); // 18
 triangles.push_back(6);
 triangles.push_back(9);
 triangles.push_back(10); // 19
 triangles.push_back(4);
 triangles.push_back(6);
 triangles.push_back(10); // 20
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/

void Sphere::subdivide()
{
  unsigned int vert_num = vertices.size();
  
  int num = triangles.size()/3; // subdivide each triangle
  for(int i=0; i<num; i++) 
  {
   unsigned int ai, bi, ci, di, ei, fi;
   ai = triangles.front();
        triangles.pop_front();
        
   bi = triangles.front();
        triangles.pop_front();
        
   ci = triangles.front();
        triangles.pop_front();

   TVector3 a = vertices[ai];
   TVector3 b = vertices[bi];
   TVector3 c = vertices[ci];

   //  d = a+b
   TVector3 d = (a + b).Unit();

   //  e = b+c
   TVector3 e = (b + c).Unit();

   //  f = c+a
   TVector3 f = (c + a).Unit();

   // add all new edge indices of new triangles to the triangles deque
   bool d_found = false;
   bool e_found = false;
   bool f_found = false;
   
   // if new vertices exist, continue
   for(unsigned int j = vert_num; j < vertices.size(); j++) 
   {
     if(vertices[j] == d) 
     {
      d_found = true;
      di = j;
      continue;
     }
     
     if(vertices[j] == e) 
     {
      e_found = true;
      ei = j;
      continue;
     }
     
     if(vertices[j] == f) 
     {
      f_found = true;
      fi = j;
      continue;
     }

   }
   // end j loop
   
   // if vertices do not exist, add them
   if(!d_found) 
   {
    di = vertices.size();
    vertices.push_back(d);
   }
   
   if(!e_found) 
   {
    ei = vertices.size();
    vertices.push_back(e);
   }

   if(!f_found) 
   {
    fi = vertices.size();
    vertices.push_back(f);
   }

   triangles.push_back(ai);
   triangles.push_back(di);
   triangles.push_back(fi);

   triangles.push_back(di);
   triangles.push_back(bi);
   triangles.push_back(ei);

   triangles.push_back(fi);
   triangles.push_back(ei);
   triangles.push_back(ci);

   triangles.push_back(fi);
   triangles.push_back(di);
   triangles.push_back(ei);
  }
  // end i loop
  
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/

void Sphere::make_unique()
{
 for(unsigned int i = 0; i<vertices.size(); i++) 
 {
  if(vertices[i].Z()<0) 
  { // make hemisphere
   vertices.erase(vertices.begin() + i);

   // delete all triangles with vertex_i
   int t = 0;
   for(auto it = triangles.begin(); it != triangles.end();) 
   {
    if(triangles[t] == i || triangles[t + 1] == i || triangles[t + 2] == i) 
    {
     it = triangles.erase(it);
     it = triangles.erase(it);
     it = triangles.erase(it);
    } 
    else 
    {
     ++it;
     ++it;
     ++it;
     t += 3;
     }
    }
    
    // update indices
    for(unsigned int j = 0; j < triangles.size(); j ++) 
    {
     if(triangles[j]>i) 
          triangles[j]--;
    }

     i--;
    } 
    else if(vertices[i].Z() == 0) 
    { 
     // make equator vectors unique
     if(vertices[i].X()<0) 
     {
      vertices.erase(vertices.begin() + i);
     
      // delete all triangles with vertex_i
      int t = 0;
      for(auto it = triangles.begin(); it != triangles.end();) 
      {
       if(triangles[t] == i || triangles[t + 1] == i || triangles[t + 2] == i) 
       {
        it = triangles.erase(it);
        it = triangles.erase(it);
        it = triangles.erase(it);
       } 
       else 
       {
        ++it;
        ++it;
        ++it;
        t += 3;
        }
       }
        // update indices
        for(unsigned int j = 0; j < triangles.size(); j ++) 
        {
         if(triangles[j]>i) 
             triangles[j]--;
        }
        i--;
      } 
      else if(vertices[i].X() == 0 && vertices[i].Y() == -1) 
      {
       vertices.erase(vertices.begin() + i);
       
       // delete all triangles with vertex_i
       int t = 0;
       for(auto it = triangles.begin(); it != triangles.end();) 
       {
        if(triangles[t] == i || triangles[t + 1] == i || triangles[t + 2] == i) 
        {
         it = triangles.erase(it);
         it = triangles.erase(it);
         it = triangles.erase(it);
        } 
        else 
        {
         ++it;
         ++it;
         ++it;
         t += 3;
        }
       }
       
       // update indices
       for(unsigned int j = 0; j < triangles.size(); j ++) 
       {
        if(triangles[j] > i)
            triangles[j]--;
       }
        i--;
      }
    }
  }
}

