
#ifndef LINE3D_H
#define LINE3D_H

// C++ headers
#include <array>
#include <vector>

// ROOT headers
#include "TVector3.h" 

#include "Point3d.h"

class Line3d : public TObject
{
 private:
     /* 
       parametric representation:
       coordinates (x,y,z) = X of a generic point 
       belonging to the line is X = A + B*t, t real
     */
     TVector3 A; 
     TVector3 B; 
     
     // total charge (sum of "charged" points)
     double total_charge;
     
     // number of points the line fits
     int total_points;
     
     // cloud of points associated to the line
     std::vector<Point3d> all_the_inliers;
     
 public:
     
     Line3d();
     Line3d(array<double, 3> intercept, array<double, 3> direction);
     Line3d(TVector3 intercept, TVector3 direction);
     Line3d(Point3d P, Point3d Q); // unique line passing through two points
     
     ~Line3d();
     
     // Setters   
     void set_intercept(array<double, 3> intercept){ A.SetXYZ(intercept[0], intercept[1], intercept[2]); } 
     void set_intercept(TVector3 intercept) { A = intercept; }  
      
     void set_direction(array<double, 3> direction){ B.SetXYZ(direction[0], direction[1], direction[2]); }
     void set_direction(TVector3 direction){ B = direction; } 
     
     void set_total_charge(double charge){ total_charge = charge; }
     void add_total_charge(double charge){ total_charge += charge; }

     void set_total_points(int points)   { total_points = points; }
     void set_inliers(std::vector<Point3d> inliers) { all_the_inliers = inliers;}
     
     // getters
     TVector3 get_intercept(){ return A; }     
     TVector3 get_direction(){ return B; }
     
     double get_total_charge() { return total_charge; }
     int get_total_points() { return total_points; }
     std::vector<Point3d> get_inliers() { return all_the_inliers; }
     
     // operators
     bool operator == (const Line3d &l) const 
     	{return (A == l.A) and (B == l.B);}
     
     friend std::ostream& operator<<(std::ostream &out, const Line3d &l)
     {
      out<<"A : ("<<l.A.X()<<", "<<l.A.Y()<<", "<<l.A.Z()<<")\n";
      out<<"B : ("<<l.B.X()<<", "<<l.B.Y()<<", "<<l.B.Z()<<")\n";
      out<<"Tot. Charge = "<<l.total_charge;
      return out;
     }
     
     // find unique line passing through two points and set parameters
     void straight_line(Point3d P, Point3d Q);  
     
     double distance_from_point(Point3d P);
     
     /*
       given a cloud of inliers, i.e. points within 
       a certain distance from the line
       perform Least Square Orthogonal regression 
       to estimate line parameters
     */
     void fit_inliers(std::vector<Point3d> inliers, TVector3 A_guess, TVector3 B_guess); 
     
     /*
       The vertex is defined as the point that 
       minimizes the sum of the distance from each line
       
       In the ideal case (lines belong to the same plane)
       it gives back the correct results
       
       In practical cases, this procedure is the best compromise
       in terms of time consumption and implementation simplicity,
       for a not-high (some units) number of lines.
     */     
     static Point3d fit_vertex(vector<Line3d> tracks, TVector3 guess);
 
     ClassDef(Line3d, 1);
};

#endif
