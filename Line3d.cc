
#include "Line3d.h"

ClassImp(Line3d);

#include "TVirtualFitter.h"
#include "TGraph2D.h" // used to "convert" the point cloud into TObject (3D graph) fed into the line fitter
#include "TMatrixD.h" // used to "convert" the lines into TObject (matrix) fed into the vertex fitter

// stardard ROOT minimizer (fitter) arguments, cannot be changed
extern void LineFitterFunction(int &npar, double *gin, double &sum, double *par, int iflag);

extern void VertexFitterFunction(int &npar, double *gin, double &sum, double *par, int iflag);

Line3d::Line3d()
{
 A.SetXYZ(0,0,0);
 B.SetXYZ(0,0,0);
 
 total_charge = 0;
 total_points = 0;
 
 all_the_inliers = {};
}

/******************************************************/
/******************************************************/
/******************************************************/

Line3d::Line3d(array<double, 3> intercept, array<double, 3> direction)
{
 A.SetXYZ(intercept[0], intercept[1], intercept[2]);
 B.SetXYZ(direction[0], direction[1], direction[2]);
 
 if(direction[0] == 0 and direction[1] == 0 and direction[2] == 0 )
 {
  std::cout<<"Error: direction vector is empty"<<std::endl;
 }
 
 B = B.Unit();
 
 total_charge = 0;
 total_points = 0;
 
 all_the_inliers = {};
}

/******************************************************/
/******************************************************/
/******************************************************/

Line3d::Line3d(TVector3 intercept, TVector3 direction)
{
 A = intercept;
 B = direction;
 
 if(B.Mag2() == 0.)
 {
  std::cout<<"Error: direction vector is empty"<<std::endl;
 }
 
 total_charge = 0;
 total_points = 0;
 
 all_the_inliers = {};
}

/******************************************************/
/******************************************************/
/******************************************************/

Line3d::Line3d(Point3d P, Point3d Q)
{
 straight_line(P,Q);
 total_charge = P.get_e() + Q.get_e();
 total_points = 2;
}

/******************************************************/
/******************************************************/
/******************************************************/

Line3d::~Line3d(){}

/******************************************************/
/******************************************************/
/******************************************************/

void Line3d::straight_line(Point3d P, Point3d Q)
{
 TVector3 p = P.get_xyz();
 A = p;

 TVector3 q = Q.get_xyz(); 
 B = p-q;
 
 B = B.Unit();
 
 total_points = 2;
 total_charge = 0;
 
 all_the_inliers.push_back(P);
 all_the_inliers.push_back(Q);
}

/******************************************************/
/******************************************************/
/******************************************************/

double Line3d::distance_from_point(Point3d P)
{
 double t = B.Dot(P.get_xyz()-A)/B.Mag2(); // Mag2 is be useless if B is normalized, left for generality
 TVector3 h = A + B*t; // projection of the point onto the line
 
 Point3d H(h,0); 
 return P.distance(H);
}

/******************************************************/
/******************************************************/
/******************************************************/

void Line3d::fit_inliers(std::vector<Point3d> inliers, TVector3 A_guess, TVector3 B_guess)
{
 // 3 direction components and 3 coordinates of the intercept
 TVirtualFitter *line_fitter = TVirtualFitter::Fitter(0,6);
 
 line_fitter->SetErrorDef(1);    // Chi - Square minimization
 
 // Int_t SetParameter (Int_t -> int, Double_t -> double etc.)
 // Arguments:
 // Int_t parNo, const char* name, Double_t initVal, Double_t initErr, Double_t lowerLimit, Double_t upperLimit

 double arglist[3];
 arglist[0] = -1;
 line_fitter->ExecuteCommand("SET PRINT", arglist, 1);
 line_fitter->ExecuteCommand("SET NOW", arglist, 1);

 line_fitter->SetParameter(0, "Ax", A_guess.X(), 0.001, 0,0);
 line_fitter->SetParameter(1, "Ay", A_guess.Y(), 0.001, 0,0);
 line_fitter->SetParameter(2, "Az", A_guess.Z(), 0.001, 0,0);
 
 line_fitter->SetParameter(3, "Bx", B_guess.X(), 0.001, 0,0);
 line_fitter->SetParameter(4, "By", B_guess.Y(), 0.001, 0,0);
 line_fitter->SetParameter(5, "Bz", B_guess.Z(), 0.001, 0,0);
  
 int N = inliers.size();
 TGraph2D cloud(N);
 for(int k=0; k<N; k++)
 {
  Point3d P = inliers[k];
  std::array<double, 4> v = P.get_xyze();  
  cloud.SetPoint(k, v[0], v[1], v[2]);
 }
 
 line_fitter->SetObjectFit(&cloud);
 line_fitter->SetFCN(LineFitterFunction);
 
 arglist[0] = 1000; // number of function calls 
 arglist[1] = 0.005; // tolerance 
 line_fitter->ExecuteCommand("MIGRAD",arglist,2);
 
 // intercept and directions after fit
 double a[3], b[3];
 
 // getting the results
 for(int k=0; k<3; k++)
     a[k] = line_fitter->GetParameter(k);
 for(int k=0; k<3; k++)
     b[k] = line_fitter->GetParameter(k+3);
     
 A.SetXYZ(a[0], a[1], a[2]);
 B.SetXYZ(b[0], b[1], b[2]);
 
 B = B.Unit();
 
 total_charge = Point3d::cloud_charge(inliers);
 total_points = inliers.size();
}

/******************************************************/

void LineFitterFunction(int &npar, double *gin, double &sum, double *par, int iflag)
{
 TVector3 intercept(par[0], par[1], par[2]);
 TVector3 direction(par[3], par[4], par[5]);
 
 Line3d temp_line(intercept, direction);
 
 TGraph2D *cloud = (TGraph2D*)(TVirtualFitter::GetFitter())->GetObjectFit();
 
 int n = cloud->GetN();
 
 double *x = cloud->GetX();
 double *y = cloud->GetY();
 double *z = cloud->GetZ();
 
 for(int k=0; k<n; k++)
 {
  Point3d P(x[k], y[k], z[k], 0);
  double d = temp_line.distance_from_point(P);
  sum += d*d; 
 }
 
}

/************************************************/
/************************************************/
/************************************************/

Point3d Line3d::fit_vertex(vector<Line3d> tracks, TVector3 guess)
{
 Point3d V(0,0,0,0);
 
 // 3: coordinates of the vertex
 TVirtualFitter *vertex_fitter = TVirtualFitter::Fitter(0,3);

 vertex_fitter->SetErrorDef(1);    // Chi-square minimization

// Int_t SetParameter (Int_t -> int, Double_t -> double etc.)
 // Arguments:
 // Int_t parNo, const char* name, Double_t initVal, Double_t initErr, Double_t lowerLimit, Double_t upperLimit

 double arglist[3];
 arglist[0] = -1;
 vertex_fitter->ExecuteCommand("SET PRINT", arglist, 1);
 vertex_fitter->ExecuteCommand("SET NOW", arglist, 1);
 
 // measurement in mm
 double guess_X = guess.X();
 double guess_Y = guess.Y();
 double guess_Z = guess.Z();
 
 vertex_fitter->SetParameter(0, "xV", guess_X, 0.0001, 0,0);
 vertex_fitter->SetParameter(1, "yV", guess_Y, 0.0001, 0,0);
 vertex_fitter->SetParameter(2, "zV", guess_Z, 0.0001, 0,0);
 
 // each rows contains the parameters of a line
 // cols 0, 1, 2 contains intercept components
 // "    3, 4, 5  "   "   direction  "    "
 const int nlines = tracks.size();
 TMatrixD lines(nlines, 6);
 
 for(int k=0; k<nlines; k++)
 {
  Line3d l = tracks[k];
  
  TVector3 a = l.get_intercept();
  TVector3 b = l.get_direction();
  
  lines[k][0] = a.X();
  lines[k][1] = a.Y();
  lines[k][2] = a.Z();
  
  lines[k][3] = b.X();
  lines[k][4] = b.Y();
  lines[k][5] = b.Z();  
 }

 vertex_fitter->SetObjectFit(&lines);
 vertex_fitter->SetFCN(VertexFitterFunction);
 
 arglist[0] = 1000; // number of function calls 
 arglist[1] = 0.005;    // tolerance 
 vertex_fitter->ExecuteCommand("MIGRAD",arglist,2);
 
 // intercept and directions after fit
 double vertex[3];
 
 // getting the results
 for(int k=0; k<3; k++)
     vertex[k] = vertex_fitter->GetParameter(k);

 V.set_xyz(vertex[0], vertex[1], vertex[2]);

 return V;
}

/************************************************/

void VertexFitterFunction(int &npar, double *gin, double &sum, double *par, int iflag)
{
 TMatrixD lines = *((TMatrixD*)(TVirtualFitter::GetFitter()->GetObjectFit())); 
 
 Point3d temp_V(par[0], par[1], par[2], 0);
 
 double chi2 = 0;
 for(int k=0; k<lines.GetNrows(); k++)
 {  
  TVector3 A(lines[k][0], lines[k][1], lines[k][2]);
  TVector3 B(lines[k][3], lines[k][4], lines[k][5]);
  
  Line3d line(A,B);
  
  double d = line.distance_from_point(temp_V);
  chi2 += d*d;
 }
 
 sum = chi2;
}


