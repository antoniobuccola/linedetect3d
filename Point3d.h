
#ifndef POINT3D_H
#define POINT3D_H

#include <iostream>	
#include <array>

#include "TObject.h"
#include "TVector3.h"

#include "TH3F.h"

#define DBGLV 0

class Point3d : public TObject
{
 private:
     TVector3 _coo; // _coordinates of the point
     double _e; // charge of the point
 
 public:
     Point3d();
     Point3d(double x, double y, double z, double e);
     Point3d(std::array<double, 4> coords);
     Point3d(TVector3 coords, double e);
     
     ~Point3d(){};
     
     // Setters
 
     void set_x(double x){ _coo.SetX(x); }
     void set_y(double y){ _coo.SetY(y); }
     void set_z(double z){ _coo.SetZ(z); }
     
     void set_xyz(double x, double y, double z)
     { 
      _coo.SetX(x);
      _coo.SetY(y);
      _coo.SetZ(z);
     }
     
     void set_xyz(array<double, 3> pos)
     {
      _coo.SetX(pos[0]);
      _coo.SetY(pos[1]);
      _coo.SetZ(pos[2]);
     }
     
     void set_xyz(TVector3 pos){ _coo = pos;}
     
     void set_xyze(array<double, 4> v)
     {
      _coo.SetX(v[0]);
      _coo.SetY(v[1]);
      _coo.SetZ(v[2]);
      _e = v[3];
     }
     
     void set_xyze(TVector3 pos, double e)
     { 
      _coo = pos;
      _e = e; 
     }     
     
     void set_e(double e){ _e = e; }
     
     // Getters
     
     double get_x(){ return _coo.X(); }
     double get_y(){ return _coo.Y(); }
     double get_z(){ return _coo.Z(); }
     
     double get_e(){ return _e; }
     
     TVector3 get_xyz() { return _coo;}
     
     array<double, 4> get_xyze() { return {_coo.X(), _coo.Y(), _coo.Z(), _e}; }
     
     // operators  
     bool operator == (const Point3d &p) const 
     	{return _coo == p._coo;}
     
     friend std::ostream& operator<<(std::ostream &out, const Point3d &p)
     {
      out<<"("<<p._coo.X()<<", "<<p._coo.Y()<<", "<<p._coo.Z()<<", "<<p._e<<")";
      return out;
     }
          
     // Euclidean distance
     double distance(Point3d q);
     
     // total energy of a cloud of points
     static double cloud_charge(vector<Point3d> cloud);
     
     /* 
       convert a 3D ROOT histogram
       into a cloud of Point3d objects
       
       x' (phys. units) = offset + scaling_x * x (arbitrary units)
       same for y' and z'
     */
     static std::vector<Point3d> convert_histogram(TH3F *histo, TVector3 offset);
              
     ClassDef(Point3d, 1);
};

#endif
