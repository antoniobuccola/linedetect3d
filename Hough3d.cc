#include "Hough3d.h"

#include <math.h>

ClassImp(Hough3d);

// non - member functions
double make_round_to_nearest(double num) 
{
 return (num > 0.0) ? floor(num + 0.5) : ceil(num - 0.5);
}

TVector3 get_max_coordinates(TH3F *data, TVector3 offset)
{
 int nx = data->GetXaxis()->GetNbins();
 int ny = data->GetYaxis()->GetNbins();
 int nz = data->GetZaxis()->GetNbins();
 
 double dx = data->GetXaxis()->GetBinWidth(1);
 double dy = data->GetYaxis()->GetBinWidth(1);
 double dz = data->GetZaxis()->GetBinWidth(1);
  
 TVector3 v(offset.X() + nx*dx, offset.Y() + ny*dy, offset.Z() + nz*dz);
 
 return v;
}

/***********************************************************************************************************/
/***********************************************************************************************************/

Hough3d::Hough3d()
{
 _initial_number_points = 0;
 _initial_energy = 0;
 
 sphere = 0;
 _d_inl = 0; 
 
 _max_distance = 0;
 
 points = {};
 inliers = {};
 
 lines = {};
}

/**************************************************************/
/**************************************************************/
/**************************************************************/

Hough3d::~Hough3d()
{
 if(sphere == NULL)
     delete sphere;
}

/**************************************************************/
/**************************************************************/
/**************************************************************/

Hough3d::Hough3d(TH3F *data, TVector3 offset)
{
 auto tmp_pts = Point3d::convert_histogram(data, offset);
 _initial_energy = Point3d::cloud_charge(tmp_pts);
 
 center = 0.5*get_max_coordinates(data, offset);
 
 _max_distance = (center*2.).Mag();
 
 for(auto Q : tmp_pts)
 {
  Point3d P;
  
  P.set_xyz(Q.get_xyz() - center);
  P.set_e(Q.get_e());
  
  points.push_back(P);
 }
 
 _d_inl = 0; 

 inliers = {};
 
 lines = {};
}

/**************************************************************/
/**************************************************************/
/**************************************************************/

Hough3d::Hough3d(TH3F *data, double inliers_distance, TVector3 offset)
{
 auto tmp_pts = Point3d::convert_histogram(data, offset);
 _initial_energy = Point3d::cloud_charge(tmp_pts);
 
 center = 0.5*get_max_coordinates(data, offset);
 
 _max_distance = (center*2.).Mag();
 
 for(auto Q : tmp_pts)
 {
  Point3d P;
  
  P.set_xyz(Q.get_xyz() - center);
  P.set_e(Q.get_e());
  
  points.push_back(P);
 }
 
 sphere = 0;
 _d_inl = inliers_distance; 
   
 inliers = {};
 
 lines = {};
}

/**************************************************************/
/**************************************************************/
/**************************************************************/

void Hough3d::set_data(TH3F* data, TVector3 offset)
{  
 auto tmp_pts = Point3d::convert_histogram(data, offset);
 _initial_energy = Point3d::cloud_charge(tmp_pts);
 
 center = 0.5*get_max_coordinates(data, offset);
 
 _max_distance = (center*2.).Mag();
 
 for(auto Q : tmp_pts)
 {
  Point3d P;
  
  P.set_xyz(Q.get_xyz() - center);
  P.set_e(Q.get_e());
  
  points.push_back(P);
 }
}

void Hough3d::discretize(double dl, int division_steps) 
{  
 if(sphere != NULL)
     return;
          
 if(dl<=0 or division_steps<=0)
 {
  std::cout<<"Error: parameters must be non-null both\n";
  return; 
 }
 if(_max_distance <= 0)
 {
  std::cout<<"Error: maximum distance has not been set (remember to set data)\n";
  return;  
 }
  
 sphere = new Sphere(division_steps);
 
 num_b = sphere->get_vertices().size();
 
 num_dist = make_round_to_nearest(2.*_max_distance/dl);
 
 std::cout<<"Prepearing the accumulator with num_dist = "<<num_dist<<"  num_b = "<<num_b<<endl;
 
 _dl = dl;
 
 voting_space.resize(num_dist*num_dist*num_b);
}

/**************************************************************/
/**************************************************************/
/**************************************************************/

void Hough3d::point_vote(Point3d P, bool add)
{
 std::vector<TVector3> vertices = sphere->get_vertices();
 
 TVector3 coo = P.get_xyz();
 
 for(unsigned int k=0; k<vertices.size(); k++)
 {
  TVector3 b = vertices[k];
  double bx = b.X();
  double by = b.Y();
  double bz = b.Z();
  
  double beta = 1./(1+bz);
  
  //x' (x_new) and y' (y_new) according to IPOL paper
  
  double x_new = (1-beta*bx*bx)*coo.X() - beta*bx*by*coo.Y() - bx*coo.Z();
  double y_new = -beta*bx*by*coo.X() + (1-beta*by*by)*coo.Y() - by*coo.Z();
  
  unsigned int xi = make_round_to_nearest((x_new + _max_distance)/_dl);
  unsigned int yi = make_round_to_nearest((y_new + _max_distance)/_dl);
  
  unsigned int index = xi*num_dist*num_b + yi*num_b + k;
  if(index <= voting_space.size())
  {
   if(add)
       voting_space[index]++;
   else
       voting_space[index]--;
  }
 }
 
}

/**************************************************************/
/**************************************************************/
/**************************************************************/

void Hough3d::find_inliers()
{
 inliers = {};
 unsigned int votes = 0;
 unsigned int index = 0;

 for(unsigned int k=0; k<voting_space.size(); k++)
 {
  unsigned int temp_votes = voting_space[k];
  if(temp_votes>votes)
  {
   votes = temp_votes;
   index = k;
  }
 }
 
 // getting x' and y' coordinates
 double x = (int)(index/(num_b*num_dist));
 index -= (int)x*num_dist*num_b;
 x = x*_dl - _max_distance;
 
 double y = (int) index/num_b;
 index -= (int) y*num_b;
 y = y*_dl - _max_distance;

 // getting approximate line parameters
 TVector3 B = sphere->get_vertices()[index];
 double bx = B.X();
 double by = B.Y();
 double bz = B.Z();
 
 double b = 1./(1.+bz);

 TVector3 A; 
     A.SetX(x*(1-bx*bx*b) + y*(-bx*by*b));
     A.SetY(x*(-bx*by*b) + y*(1-by*by*b));
     A.SetZ(-x*bx - y*by);

 Line3d tmp_line(A,B);
 for(auto P : points)
 {
  if(tmp_line.distance_from_point(P)<_d_inl)
      inliers.push_back(P);
 }

 guess_A = A;
 guess_B = B;
}

/**************************************************************/
/**************************************************************/
/**************************************************************/

void Hough3d::remove_inliers() 
{
 for(Point3d P : inliers) 
 {
  point_vote(P, false);
  
  for(Point3d Q : points)
  {
   if(P == Q)
   {
    // an inlier has been found and it will be removed from the actual points
    points.erase(std::remove(points.begin(), points.end(), Q), points.end());
    break;
   }
  }
  //loop on points end
 }
 // loop on inliers end 
}

/**************************************************************/
/**************************************************************/
/**************************************************************/

void Hough3d::process(double energy_fraction)
{
 if(sphere == NULL)
 {
  cout<<"Error: you must call discretize()\n";
  return;
 }
 
 lines = {};

 voting_space.clear();
 voting_space.resize(num_dist*num_dist*num_b);
 
 for(auto P : points)    
     point_vote(P, true);  // fill the accumulator 
 
 while(true)
 {  
  find_inliers();

  double inlier_energy = Point3d::cloud_charge(inliers); 
  if(inlier_energy < _initial_energy*energy_fraction) // avoid fitting noise
      break;
         
  Line3d line;
  line.fit_inliers(inliers, guess_A, guess_B);
  
  inliers = {};
  for(auto P : points)
  {
   if(line.distance_from_point(P) < _d_inl)
       inliers.push_back(P);
  }
  remove_inliers();  
  
  // going back to the original frame
  line.set_intercept(line.get_intercept() + center); 
  
  std::vector<Point3d> original_inliers;
  
  for(auto P : inliers)
  {
   P.set_xyz(P.get_xyz() + center);
   original_inliers.push_back(P);
  }
 
  line.set_inliers(original_inliers);
  
  lines.push_back(line);   
 }
 
}

