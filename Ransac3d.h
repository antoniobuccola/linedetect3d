#ifndef RANSAC3D_H
#define RANSAC3D_H

#include <array>
#include <vector>

#include "TObject.h"
#include "TH3F.h"
#include "TVector3.h"

#include "Point3d.h"
#include "Line3d.h"

class Ransac3d : public TObject
{
 private:
     double _d_max;  // maximum euclidean distance to give consensus
     
     int _n_tries;          // maximum number of times the seach of inliers begins     
     int _max_iterations;   // maximum number of times the algorithm "sees" the cloud of points 

     unsigned int _number_of_points; // number of points in the cloud
     unsigned int _min_points;       // minimum number of points that must give connsensus
          
     /* Cloud of points */
     std::vector<Point3d> points;  // data
     std::vector<Point3d> inliers; // found inliers at each try
     
     // Vector containing the parameters of all the detected lines (Line3d object)
     std::vector<Line3d> lines;
     
 public:
     Ransac3d();
     Ransac3d(TH3F *data, double max_distance, int max_iters, int n_tries, int min_num_points, TVector3 offset);
     
     ~Ransac3d(){};   
     
     // setters     
     void set_data(TH3F *data, TVector3 offset);
     
     void set_cloud_point(std::vector<Point3d> cloud)
          {points = cloud;}
    
     void set_inliers_distance(double max_distance)
     	{_d_max = max_distance;}
          	
     void set_max_iterations(int max_iterations)     
     	{_max_iterations = max_iterations;}
     
     void set_tries(int n_tries)
          {_n_tries = n_tries;}
     	
     void set_min_num_points(int min_num_points) 
     	{_min_points = min_num_points;}
     
     // getters 
     double get_max_distance()
          {return _d_max;}
     	
     double get_max_iterations()
          {return _max_iterations;}
     	
     double get_min_num_points()
          {return _min_points;} 
     
     // get the lines detected with RANSAC
     std::vector<Line3d> get_lines()
          {return lines;}    
     
     // get a random point from the cloud
     Point3d get_random_point();  
     
     // searching for inliers using consensus
     void search_inliers(); 
     
     // remove the inliers associated to the detected line
     void remove_inliers(); 
                                                 
     ClassDef(Ransac3d, 1);   
};

#endif
