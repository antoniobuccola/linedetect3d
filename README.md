**Line detection in three dimensions**

* classes written to work in the **ROOT** framework (https://root.cern.ch/)

  **Remember**: different C++ implementations already exist. 

* all classes inherited from TObject class

* custom class members are self-explanatory or explained in-file
 

**Point3d** 

   class implementing a point in the 3D space carrying a charge, 
   i.e. 4 coordinates in total.

**Line3d**

   class implementing a line in the 3D space using parametric representation: 
   
   **X = A + Bt**
    
   **A**: intercept (or position) vector
    
   **B**: (normalized) direction vectors 
    
   Least Square Orthogonal (LSO) regression is used to estimate 
   the parameters of a line from a cloud of Point3d objects.
    
   *Note*: **Minuit** is employed for minimization
   
   https://root.cern.ch/doc/master/classTMinuit.html
    
**Sphere**
  
   implementation of the direction discretization
     
   http://www.ipol.im/pub/art/2017/208/?utm_source=doi
   
   adapted to employ TVector3 ROOT class

**Algorithms for 3D line detection**

**Ransac3d**

   implementation of the RANdom SAmple Consensus (RANSAC) algorithm 
    
   Some info: *https://en.wikipedia.org/wiki/Random_sample_consensus*

**Hough3d**

   implementation of the Hough Transform (to fit ROOT environment)
   
   Some info: *http://www.ipol.im/pub/art/2017/208/?utm_source=doi*
    


	
