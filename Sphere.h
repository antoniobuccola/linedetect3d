// Credit: source code referenced in the README.md file

#ifndef SPHERE_H
#define SPHERE_H

#include <vector>
#include <deque>

#include "TVector3.h"

class Sphere : public TObject
{
 private:
 
     std::vector<TVector3> vertices; // vertices of the (subdivided) icosahedron
     
     std::deque<unsigned int> triangles; // triangles of the surface of the icosahedron
 
 public:
 
     Sphere();
     Sphere(int subdivsions);
        
     ~Sphere(){};
     
     std::vector<TVector3> get_vertices() { return vertices; }
     
     void create_icosahedron(); // create the starting icosahedron
     
     void subdivide(); // perform one subdivision step
     
     void make_unique(); // remove redundancy in the direction vectors
         
 ClassDef(Sphere, 1);
};


#endif
