#include "Ransac3d.h"

#include "TVector3.h"
#include "TRandom3.h"

ClassImp(Ransac3d);

Ransac3d::Ransac3d()
{
 _d_max = 0;
 _max_iterations = 0;
 _n_tries = 0;
 _min_points = 0;
 _number_of_points = 0;
     
 points = {}; 
 inliers = {}; 
     
 lines = {};
}

/***************************************************************/
/***************************************************************/
/***************************************************************/

Ransac3d::Ransac3d(TH3F *data, double max_distance, int max_iters, int n_tries, int min_num_points, TVector3 offset)
{
 _d_max = max_distance;
 _max_iterations = max_iters;
 _n_tries = n_tries;
 _min_points = min_num_points;
     
 points = Point3d::convert_histogram(data, offset); 

 _number_of_points = points.size();
 inliers = {}; 
     
 lines = {}; 
}

/***************************************************************/
/***************************************************************/
/***************************************************************/

void Ransac3d::set_data(TH3F *data, TVector3 offset)
{
 points = Point3d::convert_histogram(data, offset); 
 _number_of_points = points.size();
}

/***************************************************************/
/***************************************************************/
/***************************************************************/

Point3d Ransac3d::get_random_point()
{
 TRandom3 x(0);
 int index = (int)(x.Uniform(0,_number_of_points)+1);

 return points[index];
}

/***************************************************************/
/***************************************************************/
/***************************************************************/

void Ransac3d::search_inliers() 
{
 lines = {};

 int tries = _n_tries;
 
 while(tries>=0)
 {
  double chi2 = 1e6;
  
  TVector3 guess_A;
  TVector3 guess_B;

  int iterations = _max_iterations;  
  while(iterations >= 0 && _number_of_points >= _min_points)
  {  
   Point3d P = get_random_point();
   Point3d Q;
   do
     Q = get_random_point();
   while(P == Q);
   
   Line3d maybe_line(P,Q); // straight line passing through the selected points
   
   std::vector<Point3d> maybe_inliers = {};
   double maybe_chi2 = 0;
  
   for(Point3d R : points)
   {
    double d = maybe_line.distance_from_point(R);
    if(d <= _d_max)
    {
     maybe_inliers.push_back(R);	
     maybe_chi2 += d*d;
    }
   }
   
   if(maybe_inliers.size() >= _min_points) // reached enough consensus
   {
    if(maybe_chi2 < chi2)  // got better inliers
    {
     inliers = maybe_inliers;
     chi2 = maybe_chi2;
     
     // saving guess for LSO regression of inliers
     guess_A = maybe_line.get_intercept(); 
     guess_B = maybe_line.get_direction();
    }
   }
  
   iterations--;
  }
  // end loop on iterations
    
  if(inliers.size()>0)
  {
   Line3d detected_line;
   detected_line.fit_inliers(inliers, guess_A, guess_B);
   lines.push_back(detected_line);
   remove_inliers();
  }
  
  inliers = {};
  
  tries--; 
 }
 // end loop on tries 
}

/***************************************************************/
/***************************************************************/
/***************************************************************/

void Ransac3d::remove_inliers() 
{
 for(Point3d P : inliers) 
 {
  for(Point3d Q : points)
  {
   if(P == Q)
   {
    // an inlier has been found and it will be removed from the actual points
    points.erase(std::remove(points.begin(), points.end(), Q), points.end());
    break;
   }
  }
  //loop on points end
 }
 // loop on inliers end 
}
