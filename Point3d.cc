
#include "Point3d.h"

#include "TMath.h"

ClassImp(Point3d);

Point3d::Point3d()
{
 _coo.SetXYZ(-1,-1,-1);
 _e = -1;
}

/************************************************/
/************************************************/
/************************************************/

Point3d::Point3d(double x, double y, double z, double e)
{
 _coo.SetXYZ(x,y,z);
 _e = e;
}

/************************************************/
/************************************************/
/************************************************/

Point3d::Point3d(array<double, 4> coords)
{
 _coo.SetXYZ(coords[0], coords[1], coords[2]);
 _e = coords[3];
}

/************************************************/
/************************************************/
/************************************************/

Point3d::Point3d(TVector3 coords, double e)
{
 _coo = coords;
 _e = e;
}

/************************************************/
/************************************************/
/************************************************/

double Point3d::distance(Point3d q)
{
 TVector3 d = _coo - q.get_xyz();
 return d.Mag();
}

/************************************************/
/************************************************/
/************************************************/

std::vector<Point3d> Point3d::convert_histogram(TH3F *histo, TVector3 offset)
{
 std::vector<Point3d> cloud;
 
 int nbinsx = histo->GetXaxis()->GetNbins();
 int nbinsy = histo->GetYaxis()->GetNbins();
 int nbinsz = histo->GetZaxis()->GetNbins();
 
 double dx = histo->GetXaxis()->GetBinWidth(1);
 double dy = histo->GetYaxis()->GetBinWidth(1);
 double dz = histo->GetZaxis()->GetBinWidth(1);
   
 for(int xx = 1; xx <= nbinsx; xx++)
 {
  for(int yy = 1; yy <= nbinsy; yy++)
  {
   for(int zz = 1; zz <= nbinsz; zz++)
   {
    int cbin = histo->GetBin(xx, yy, zz);
   
    double c = histo->GetBinContent(cbin);
    if(c>0)
    {
     double x = offset.X() + (xx-1)*dx;
     double y = offset.Y() + (yy-1)*dy; 
     double z = offset.Z() + (zz-1)*dz;
    
     double e = c;
    
     cloud.emplace_back(x,y,z,e);
    }
   }
  }
 }
 
 return cloud;
}

/************************************************/
/************************************************/
/************************************************/

double Point3d::cloud_charge(vector<Point3d> cloud)
{
 double total_charge = 0;
 
 for(auto P : cloud)
     total_charge += P.get_e();
     
 return total_charge;
}

