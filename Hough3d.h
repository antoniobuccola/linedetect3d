#ifndef HOUGH3D_H
#define HOUGH3D_H

#include <vector>
#include <array>
#include <map>
#include <utility>

#include "TObject.h"
#include "TH3F.h"

#include "Point3d.h"
#include "Line3d.h"
#include "Sphere.h"

class Hough3d : public TObject
{
 private:
        
    unsigned int _initial_number_points; // initial number of points of the cloud
        
    double _d_inl;  // maximum distance for a point to be considered an inlier of the detected line
 
    double _max_distance; // maximum euclidean distance from the origin
    
    double _initial_energy; // initial energy (charge) associated to an event
               
    std::vector<Point3d> points;  // data 
    std::vector<Point3d> inliers; // found inliers  
    
    Sphere *sphere; // needed for parameter direction discretization
    
    int num_b, num_dist; 
    double _dl; 
    
    TVector3 center;
    
    std::vector<unsigned int> voting_space;
    unsigned int hough_maximum; // index of the maximum 
    
    TVector3 guess_A; // guess for line fitting
    TVector3 guess_B;
    
    /* 
        Vector containing the parameters of all the detected lines
        
        quality factor (the lower, the better) 
        slope, 
        intercept, 
        number of fitted points,
        total energy
     */
    std::vector<Line3d> lines; 
     
 public:
    Hough3d();   
    Hough3d(TH3F *data, TVector3 offset); 
    Hough3d(TH3F *data, double inliers_distance, TVector3 offset);
    
    ~Hough3d();
    
    // setters        
    void set_data(TH3F *data, TVector3 offset);
    
    void set_cloud_point(std::vector<Point3d> cloud)
          {points = cloud;}
    
    void set_inliers_distance(double inliers_distance)
          {_d_inl = inliers_distance;}  
    
    // getters
    double get_inliers_distance()
          {return _d_inl;}

    std::vector<Line3d> get_lines()
          {return lines;}
     
    /*
      find inliers starting from parameters
      of the maximum voted accumulator
    */
    void find_inliers();
       
    // member functions
    /*
      create parameter_space
      
      arguments
      
      dl: length step for the discretization of the (transformed) coordinates
      
      division_steps: steps for recursive discretization of directions
    */
    void discretize(double dl, int division_steps);
    
    /*
      fill the accumulator 
      with point voting
    */
    void point_vote(Point3d P, bool add);
    
    // remove inliers
    void remove_inliers();
    
    // process the data to find all the lines
    void process(double energy_fraction);
    
    ClassDef(Hough3d, 1);
};

#endif








